# Mate Tutor 



## **Descripcion**

El Tutor Inteligente Móvil  apoya al docente en la enseñanza de Matemática Financiera I.

### Beneficios

- Realizar pruebas de diagnóstico.

- Diseñar una rutina de práctica constante.

- Retroalimentar conceptos no comprendidos.

- Implementar características de gamificación.

- Brindar acceso a recursos de enseñanza.
