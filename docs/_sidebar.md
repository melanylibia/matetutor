<!-- docs/_sidebar.md -->

* [Home](/)
* [instalacion](instalacion.md)
* [estudiante](estudiante.md)
* [profesor](profesor.md)
* [ejercicios](ejercicios/index.md)
    * [porcentajes](ejercicios/porcentajes/index.md)
    * [tipos_impuestos](ejercicios/tipos_impuestos/index.md)
    
