# Profesor View/Scene Documentacion

## Visión general

En el videojuego la vista/escena consiste principalmente en la gestion de los estudiantes. Esta documentación proporciona una visión general de los elementos que suelen encontrarse dentro de la vista/escena, 

## Requerimientos

- Tener una el permiso del profesor.
- crear usuario: se relaciona a la vista de creacion de un nuevo estudiante.
   
   ![](./images/login.jpg)

   ![](./images/crear-usuario.jpg)
