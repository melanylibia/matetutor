Si el precio del pasaje se aumenta en un 10% y ahora cuesta 10Bs, ¿cuál sera su precio original?

$\%  \text{ Cantidad} \\$

$100 - 10 \\$

$110  - X \\$

$X =  \frac{110 \times 10}{100} = 11 \\$

