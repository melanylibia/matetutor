
Si el 25% de un número es 30, ¿cuál es el número? 

$\%  \text{ Cantidad} \\$

$100 - X \\$

$25  - 30 \\$

$X =  \frac{100 \times 30}{25} = 120 \\$

