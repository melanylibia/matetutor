Si un empleado gana un aumento del 8% y ahora gana 2,160Bs mensuales, ¿cuál era su salario anterior?

$\%  \text{ Cantidad} \\$

$108 - 2160 \\$

$100  - X \\$

$X =  \frac{100 \times 2160}{108} = 2000 \\$
