Si un artículo cuesta 50Bs y aumenta su precio en un 25%, ¿cuál será el nuevo precio?

$\%  \text{ Cantidad} \\$

$100 - 50 \\$

$125 - X \\$

$X = \frac{125 \times 50}{100} = 62.5 \\`$

