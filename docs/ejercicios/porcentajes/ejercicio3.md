
Si un descuento del 15% en una compra equivale a 30Bs, ¿cuál es el precio original del artículo?

$\%  \text{ Cantidad} \\$

$100 - X \\$

$15 - 30 \\$

$X = \frac{100 \times 30}{15} = 200 \\$
