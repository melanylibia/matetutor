Si un estudiante responde correctamente el 80% de las preguntas de un examen que tiene 50 preguntas en total, ¿cuántas preguntas respondió correctamente?


$\%  \text{ Cantidad} \\$

$100 - 50 \\$

$80 - X \\$

$X =  \frac{80 \times 50}{100} = 40 \\$

