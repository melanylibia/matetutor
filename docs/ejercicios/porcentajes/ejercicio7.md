Si un artículo se vende con un descuento del 10% y su precio de venta es de 80Bs, ¿cuál es el costo original del artículo?


$\%  \text{ Cantidad} \\$

$100 - X \\$

$90  - 80 \\$

$X =  \frac{100 \times 80}{10} = 89 \\$

