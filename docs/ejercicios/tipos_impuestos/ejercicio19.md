¿Cuáles son las desventajas del Régimen General?

Llevas registros contables, obligatoriamente.

Las multas pueden ser muy elevadas en los casos de no tener tu contabilidad bien realizada.

El mantenimiento de valor e intereses incrementan si pagas con demora tus impuestos