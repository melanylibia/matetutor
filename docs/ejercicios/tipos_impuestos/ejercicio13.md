
¿Cuáles son los elementos del Tributo?

En el diseño del sistema tributario boliviano se analizan los elementos que conforman el tributo y estos son:

1. Hecho generador: El acontecimiento que da origen a la obligación tributaria, establecido por Ley Nº 2492, Sección II, Artículo 16.

2. Sujetos: Se tienen dos roles:

    a. Pasivo: Es el contribuyente que debe cumplir las obligaciones tributarias.

    b. Activo: Está constituido por cualquiera de las administraciones tributarias que tienen las facultades de recaudación, control, verificación, determinación, ejecución y otras. (Ley Nº 2492, Sección III, Artículo 21 y Artículo 22).

5. Base imponible: Es el valor sobre el cuál se aplica la alícuota para establecer el monto a pagar. (Ley Nº 2492, Sección IV, Artículo 42).

6. Alícuota: Porcentajes o un valor que determinan el tributo a pagar aplicándose a la base imponible. (Ley Nº 2492, Sección IV, Artículo 46).
