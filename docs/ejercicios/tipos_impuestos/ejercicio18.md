¿Quiénes NO están incluidas en el Régimen General?


1. RÉGIMEN GENERAL

Comprende a todas aquellas personas naturales jurídicas que realizan actividades económicas habituales. Se incorporan también los contribuyentes directos o independientes que perciben ingresos por cuenta propia.