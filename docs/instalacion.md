### Instalar

1. Descargar la app de itch.io.

![itch.io](./images/matetutor.png)

### [Mate Tutor](https://libidevgame.itch.io/matetutor)
 
2. Ejecutar el APK.

Te aparecerá la tabla de información con todos los elementos de tu móvil a los que va a acceder la app, en caso de MateTutor no se tiene ningun acceso a la camara ni archivos personales, sólo tendrás que pulsar en Instalar para proceder a instalarla. 

**Nota** Instalar aplicaciones de fuentes desconocidas es peligroso.
