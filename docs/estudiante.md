# Estudiante Vista/Scene Documentación

## Visión general

En el videojuego la vista/escena consiste principalmente en el entorno virtual en el que los jugadores interactúan con un vegetal digital. Esta documentación proporciona una visión general de los elementos que suelen encontrarse dentro de la vista/escena, 

## Requerimientos

- Tener una cuenta
- Si tienes una cuenta sólo tienes que iniciar sesión.

## Componentes de la interfaz de usuario

### Esta área muestra la zanahoria mascota algunos indicadores.

   - **Animaciones**: La zanahoria puede crecer varias animaciones expresas 

### 2. Panel de preguntas

   - **Botones**: Acciones y características relacionadas con el cuidado de su.  Botones comunes incluyen:
     - Lapiz: instrucción extra de cómo rellenar los campos.
     - Interes: campo para completar el interes, antes de hacer la opración dividir por 100.
     - capital: campo para el capital.
     - capital inicial: campo para el capital inicial.
     - tiempo: campo para el tiempo.

### 3. Indicadores de estado
   -  **Nivel de agua**: Representa el agua para la zanahoria , el valor no puede ser negativo.
   -  **Reloj**: Representa el tiempo actual desde el inicio de la lección.
   
   ![](https://proyectos-lib.casillero.laotrared.net/mateTutor/Indicadores00.jpg)

   ![](https://proyectos-lib.casillero.laotrared.net/mateTutor/Indicadores01.jpg)

### 4. Notificaciones
   - **Alertas**: mejorar las alertas de nivel de agua.

## Funciones Interactivas

### Cambiar estado de la Música

   - La música de fondo puede sonar pero el alumno puede silenciarla con este botón.

### Salir del Juego
   - Salir del juego

### Mapa del nivel
   - muestra información del nivel del estudiante.

## Conclusión

Esta escena ofrece a los jugadores una experiencia inmersiva e interactiva en la que pueden cuidar y nutrir su virtual zanagoria o character.
